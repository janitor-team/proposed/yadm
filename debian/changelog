yadm (3.2.1-1) unstable; urgency=medium

  * New upstream version 3.2.1
  * debian/patches/use-bash-shebang: Fix bash term in posix shell.
  * debian/rules: Use DESTDIR instead of only PREFIX.
  * debian/copyright: Update copyright years.
  * Bump Standards-Version to 4.6.0 (no changes).

 -- Angel Abad <angel@debian.org>  Sat, 16 Apr 2022 13:03:53 +0200

yadm (3.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 29 Aug 2021 07:10:13 +0800

yadm (3.0.2-2) unstable; urgency=medium

  * Add upstream patch to fix bash tab completion.  (Closes: #980754)

 -- Russ Allbery <rra@debian.org>  Sat, 13 Feb 2021 09:01:05 -0800

yadm (3.0.2-1) unstable; urgency=medium

  * New upstream version 3.0.2.
  * debian/watch: Upgrade to version 4.
  * debian/NEWS: Update breaking changes info.
  * debian/salsa-ci.yml: Add salsa ci basic config.
  * debian/gbp.conf: Add gbp configuration file.
  * Update shell completion install files.
  * Declare compliance with Debian Policy 4.5.1.

 -- Angel Abad <angel@debian.org>  Sun, 31 Jan 2021 10:48:31 +0100

yadm (2.5.0-2) unstable; urgency=medium

  * Source only upload

 -- Angel Abad <angel@debian.org>  Thu, 20 Aug 2020 09:17:04 +0200

yadm (2.5.0-1) unstable; urgency=medium

  * New upstream version 2.5.0
  * debian/rules: Use execute_after_dh_auto_install

 -- Angel Abad <angel@debian.org>  Wed, 19 Aug 2020 16:42:24 +0200

yadm (2.4.0-2) unstable; urgency=medium

  * Update to debhelper compatibility level V13.

 -- Russ Allbery <rra@debian.org>  Tue, 28 Apr 2020 18:56:20 -0700

yadm (2.4.0-1) unstable; urgency=medium

  * Update to new upstream version 2.4.0.
    - Support multiple keys in yadm.gpg-recipient
    - Ensure all templates are written atomically
    - Add encrypt_with_checksums to the hooks collection
    - Escape white space in YADM_HOOK_FULL_COMMAND
    - Improve parsing of os-release
    - Improve identification of WSL
    - Write encrypt-based exclusions during decrypt
  * Install contrib/hooks as usr/share/doc/yadm/examples/hooks.

 -- Russ Allbery <rra@debian.org>  Thu, 27 Feb 2020 20:27:16 -0800

yadm (2.3.0-2) unstable; urgency=medium

  [ Angel Abad ]
  * Add debian/NEWS documenting the upgrade from 1.x versions.
  * Add myself to Uploaders and copyright.

  [ Russ Allbery ]
  * Upload to Debian with new maintainers.
  * Add myself to uploaders.
  * Run wrap-and-sort -ast.
  * Update standards version to 4.5.0 (no changes required).

 -- Russ Allbery <rra@debian.org>  Tue, 25 Feb 2020 09:33:17 -0800

yadm (2.3.0-1) unstable; urgency=medium

  * Local package for eyrie.org repository.
  * Update to new upstream version 2.3.0.
  * Update build commands to use the upstream make install target but
    remove some files that are installed by the packaging in other ways.
  * Remove build dependencies for the tests since they require Docker and
    thus are disabled.
  * Update to debhelper compatibility level V12.
    - Depend on debhelper-compat instead of using debian/compat.
  * Update to standards version 4.4.1.
    - Add Rules-Requires-Root: no.

 -- Russ Allbery <rra@debian.org>  Fri, 17 Jan 2020 11:51:37 -0800

yadm (1.12.0-2) unstable; urgency=medium

  * Bump Standards-Version to 4.2.0.
  * Bump compat to 11.
  * Add git to Depends.
  * Change Vcs-* fields to salsa.debian.org.
  * Install bash/zsh auto completion.
  * Add debian/upstream/metadata.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 09 Aug 2018 15:45:50 +0800

yadm (1.12.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.1.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sat, 28 Oct 2017 12:21:58 +0800

yadm (1.11.1-1) unstable; urgency=high

  * New upstream release.
    * Fix CVE-2017-11353 (Closes: #868300).

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Mon, 28 Aug 2017 18:33:06 +0800

yadm (1.11.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.0.0.
    * Use https in Format in d/copyright.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Mon, 17 Jul 2017 12:37:06 +0800

yadm (1.10.0-1) unstable; urgency=medium

  * Merge back to unstable.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sat, 17 Jun 2017 22:56:14 +0800

yadm (1.10.0-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sat, 20 May 2017 10:54:52 +0800

yadm (1.09-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 07 May 2017 18:11:54 +0800

yadm (1.08-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 05 Apr 2017 10:28:13 +0800

yadm (1.07-1~exp2) experimental; urgency=medium

  * Add Multi-Arch: foreign.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 26 Mar 2017 14:12:21 +0800

yadm (1.07-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Mon, 13 Feb 2017 18:43:39 +0800

yadm (1.06-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 22 Jan 2017 20:34:20 +0800

yadm (1.05-1) unstable; urgency=medium

  * New upstream release.
  * Bump compat level to 10.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Mon, 19 Sep 2016 19:09:21 +0800

yadm (1.04-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Update Vcs-* fields.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 27 Apr 2016 20:19:33 +0800

yadm (1.03-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Mon, 18 Jan 2016 19:34:20 +0800

yadm (1.02-2) unstable; urgency=low

  * fix manpage-has-errors-from-man warning.

 -- Yao-Po Wang <blue119@gmail.com>  Sat, 07 Nov 2015 13:08:12 +0800

yadm (1.02-1) unstable; urgency=low

  * Initial release. Closes: #800360

 -- Yao-Po Wang <blue119@gmail.com>  Tue, 06 Oct 2015 20:40:25 +0800
